# Dexes / iSHARE

[gitlab.com/dexes.eu/catalog-sdk](https://gitlab.com/dexes.eu/dexes-ishare)

Dexes package providing an extension of the drupal/openid_connect module with some iSHARE specific functionality.

## License

View the `LICENSE.md` file for licensing details.

## Installation

Installation of ...

```shell
composer require dexes/ishare
```
